*** Settings ***
Resource    keywords.robot

*** Test Cases ***
Open Login Page
    [Documentation]  Users are directed to login page
    Set Selenium Speed    0.5 seconds
    Login Page Should Be Open

Invalid Login
    [Documentation]  This is an Invalid Login scenario
    [Tags]  InValid_Login     Testcase    No-login
    Continue
    Wait Until Page Contains Element    //*[@id="loginForm"]/div[2]//*[text()='Please enter your username']
    ${currentUrl}    Get Location
    Should Be Equal     ${currentUrl}   https://release-test.alex4im.com/login?redirect=

Valid Login
    [Documentation]  This is a Valid Login scenario
    [Tags]  Valid_Login     Testcase    admin-login
    Input Username    ${USERNAME}
    Continue
    Input Password    ${PASSWORD}
    Submit Credentials
    Welcome Page Should Be Open
    [Teardown]   Go To Home Page


Log Out And Log In Again
    Log Out
    Input Username    ${USERNAME}
    Continue
    Input Password    ${PASSWORD}
    Submit Credentials
    Welcome Page Should Be Open
    [Teardown]   Go To Home Page



